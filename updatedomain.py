#!/usr/bin/env python3

import ovh
import argparse

subdomain = ''

 # Argument parsing

parser = argparse.ArgumentParser(description="Update A record for @ in OVH DNS")

parser.add_argument('domain', type=str,
                    help='The domain name to be updated')
parser.add_argument('IP',type=str,
                    help='IP address to put in the DNS record')
args = parser.parse_args()

domain=args.domain
IP=args.IP

 # Get handles from OVH API
 # Useful doc: https://eu.api.ovh.com/console/#/domain/zone/%7BzoneName%7D/refresh#POST

client = ovh.Client()

result = client.get('/domain/zone/%s/record' % domain,
    fieldType='A',
    subDomain=subdomain
)

assert len(result) == 1, 'More than one A record for %s' % domain
record_id = int(result[0])
assert record_id > 0, 'Identifier %s for %s @ is less than 0' % (record_id,domain)
print("%s has the following ID for @: %s" % (domain, record_id))

result = client.get('/domain/zone/%s/record/%s' % (domain, record_id))

print("Record found:",result,"Now updating record with ip %s" % IP)

 # Change record

result = client.put('/domain/zone/%s/record/%s' % (domain, record_id),
    subDomain=subdomain,
    target=IP,
    ttl=300
)

print("Request sent. Answer:",result,"Now refreshing the zone")

refresh_result = client.post('/domain/zone/%s/refresh' % domain)
new_record = client.get('/domain/zone/%s/record/%s' % (domain, record_id))

print("Request sent. Answer and new record:", refresh_result, new_record)


